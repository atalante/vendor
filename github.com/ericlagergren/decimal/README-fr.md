# decimal [![Build Status](https://travis-ci.org/ericlagergren/decimal.png?branch=master)](https://travis-ci.org/ericlagergren/decimal) [![GoDoc](https://godoc.org/github.com/ericlagergren/decimal?status.svg)](https://godoc.org/github.com/ericlagergren/decimal)

Decimal est un logithèque décimal virgule flottante, avec haute performance et
précision arbitrarie que mettre en œuvre la spécification
[General Decimal Arithmetic](http://speleotrove.com/decimal/).

## Les fonctions

 * Valeur zéros sont sûr utiliser.
 * Multiple modes de fonctionnement 
 * Haute performance
 * Un logithèque mathématiques avec « sqrt », « exp », « log », fraction continue, et beaucoup plus.
 * Un API familier idiomatique.

## Installation

`go get github.com/ericlagergren/decimal`

## Documentation

http://godoc.org/github.com/ericlagergren/decimal

## Gestion de versions

`decimal` utilise le gestion sémantique de version.

## Licence

La [BSD 3-clause](https://github.com/ericlagergren/decimal/blob/master/LICENSE)
