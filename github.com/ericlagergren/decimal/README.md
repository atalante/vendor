# decimal [![Build Status](https://travis-ci.org/ericlagergren/decimal.png?branch=master)](https://travis-ci.org/ericlagergren/decimal) [![GoDoc](https://godoc.org/github.com/ericlagergren/decimal?status.svg)](https://godoc.org/github.com/ericlagergren/decimal)

`decimal` is a high-performance, arbitrary precision, floating-point decimal
library implementing the [General Decimal Arithmetic](http://speleotrove.com/decimal/) specification.

## Features

 * Zero-values are safe to use without initialization.
 * Multiple operating modes (GDA, Go) to fit your use cases.
 * High performance.
 * A math library with `sqrt`, `exp`, `log`, continued fractions, and more.
 * A familiar, idiomatic API.

## Installation

`go get github.com/ericlagergren/decimal`

## Documentation

http://godoc.org/github.com/ericlagergren/decimal

## Versioning

`decimal` uses Semantic Versioning. 

## License

The [BSD 3-clause](https://github.com/ericlagergren/decimal/blob/master/LICENSE)
